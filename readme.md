#paint_gallery

site internet de présentation de peintures sur le thème de la mer 

## Environnement de developpement 
### pré-requis
PHP 7.4
Composer
Symfony CLI
Docker 
Docker-compose
node js et npm


Vérifier les pré-requis sauf Docker et Docker-Compose avec la commande suivante :

```bash
symfony check:requirements
```

### Lancer l'envrionnement de dev 


```bash
composer install
npm run build
docker-compose up -d
symfony serve -d
```

### Ajouter données de tests
```bash
symfony console doctrine:fixtures:load
```

### Lancer les tests

```bash
php ./vendor/bin/phpunit 
```

## Production

### Envoi des mails de contacts

Les mails de prise de contacts sont stockés en base, mettre en place un cron sur : 

```bash
symfony cconsole app:send-contact
```
