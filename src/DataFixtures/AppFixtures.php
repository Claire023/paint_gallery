<?php

namespace App\DataFixtures;

use App\Entity\Blogpost;
use App\Entity\Peinture;
use App\Entity\Categorie;
use Faker\Factory;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\User;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {

        //utilisation faker
        $faker = Factory::create('fr_FR');

        //creation fake user
        $user = new User();


        $user->setEmail('user@test.com')
            ->setPrenom($faker->firstName())
            ->setNom($faker->lastName())
            ->setTelephone($faker->phoneNumber())
            ->setAPropos($faker->text())
            ->setRoles(['ROLE_PEINTRE'])
            ->setInstagram('instagram');
        $password = $this->encoder->hashPassword($user, 'password');
        $user->setPassword($password);

        $manager->persist($user);


        //creation de 10 Blogpost
        for ($i = 0; $i < 10; $i++) {
            $blogpost = new Blogpost();
            $blogpost->setTitre($faker->words(3, true))
            ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
            ->setContenu($faker->text(350))
            ->setSlug($faker->slug(3))
            ->setUser($user);
            $manager->persist($blogpost);
        }

        //creation d'un blogpost pour les tests
        $blogpost = new Blogpost();

        $blogpost->setTitre('Blogost test')
            ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
            ->setContenu($faker->text(350))
            ->setSlug('blogpost-test')
            ->setUser($user);
            $manager->persist($blogpost);



        //creation des categories
        for ($j = 0; $j < 5; $j++) {
            $categorie = new Categorie();

            $categorie->setNom($faker->text(12))
            ->setDescription($faker->words(10, true))
            ->setSlug($faker->slug());
            $manager->persist($categorie);
        }

        $categorie = new Categorie();

        $categorie->setNom('categorie test')
        ->setDescription($faker->words(10, true))
        ->setSlug('categorie-test');

        $manager->persist($categorie);



        //creation de 2 peintures par categories
        for ($k = 0; $k < 2; $k++) {
            $peinture = new Peinture();

            $peinture->setNom($faker->words(3, true))
                    ->setLargeur($faker->randomFloat(2, 20, 60))
                    ->setHauteur($faker->randomFloat(2, 20, 60))
                    ->setEnVente($faker->randomElement([true, false]))
                    ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setDescription($faker->text())
                    ->setPortfolio($faker->randomElement([true,false]))
                    ->setSlug($faker->slug())
                    ->setFile('build/images/cover-19.jpg')
                    ->addCategorie($categorie)
                    ->setPrix($faker->randomFloat(2, 20, 60))
                    ->setUser($user);
            $manager->persist($peinture);
        }

        //peinture pour tests
        $peinture = new Peinture();

        $peinture->setNom('test peinture')
        ->setLargeur($faker->randomFloat(2, 20, 60))
        ->setHauteur($faker->randomFloat(2, 20, 60))
        ->setEnVente($faker->randomElement([true, false]))
        ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
        ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
        ->setDescription($faker->text())
        ->setPortfolio($faker->randomElement([true,false]))
        ->setSlug('peinture-test')
        ->setFile('build/images/cover-19.jpg')
        ->addCategorie($categorie)
        ->setPrix($faker->randomFloat(2, 20, 60))
        ->setUser($user);

    $manager->persist($peinture);











        $manager->flush();
    }
}
