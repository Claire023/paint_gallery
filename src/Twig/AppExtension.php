<?php

namespace App\Twig;

use App\Repository\CategorieRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private $categoryRepository;

    public function __construct(CategorieRepository $categorieRepository)
    {
        $this->categoryRepository = $categorieRepository;
    }


    public function getFunctions()
    {
        return [
            new TwigFunction('categorieNavbar', [$this, 'categorie']),
        ];
    }


    public function categorie(): array
    {
        return $this->categoryRepository->findAll();
    }
}
