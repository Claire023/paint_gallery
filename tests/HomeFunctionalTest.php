<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
// use Symfony\Component\Panther\PantherTestCase;

class HomeFunctionalTest extends WebTestCase
{
    public function testShouldDisplayHomepage()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Nom Artiste');
        $this->assertSelectorTextContains('p', 'Artiste Peintre à Montpellier');
    }
}
