<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PeintureFunctionalTest extends WebTestCase
{
    public function testShouldDisplayPeinture()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/realisations');

        $this->assertResponseIsSuccessful();
    }

    public function testShouldDisplayOnePeinture()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/realisations/peinture-test');

        $this->assertResponseIsSuccessful();
    }
}
