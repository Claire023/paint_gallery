<?php

namespace App\Tests;

use App\Entity\Blogpost;
use PHPUnit\Framework\TestCase;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use DateTime;

class CommentaireUnitTest extends TestCase
{

    public function testIsTrue()
    {
        $commentaire = new Commentaire();
        $blogpost = new Blogpost();
        $peinture = new Peinture();
        $datetime = new DateTime();

        $commentaire->setAuteur('auteur')
                    ->setEmail('email@test.com')
                    ->setContenu('contenu')
                    ->setCreatedAt($datetime)
                    ->setBlogpost($blogpost)
                    ->setPeinture($peinture);

        
        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getEmail() === 'email@test.com');
        $this->assertTrue($commentaire->getCreatedAt() === $datetime);
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getBlogpost() === $blogpost);
        $this->assertTrue($commentaire->getPeinture() === $peinture);
       
    }


    public function testIsFalse()
    {
        $commentaire = new Commentaire();
        $blogpost = new Blogpost();
        $peinture = new Peinture();
        $datetime = new DateTime();

        $commentaire->setAuteur('auteur')
                    ->setEmail('email@test.com')
                    ->setContenu('contenu')
                    ->setCreatedAt($datetime)
                    ->setBlogpost($blogpost)
                    ->setPeinture($peinture);
             
      
               
        $this->assertFalse($commentaire->getAuteur() === 'false');
        $this->assertFalse($commentaire->getEmail() === 'false');
        $this->assertFalse($commentaire->getCreatedAt() === new Datetime());
        $this->assertFalse($commentaire->getContenu() === 'false');
        $this->assertFalse($commentaire->getBlogpost() === new Blogpost());
        $this->assertFalse($commentaire->getPeinture() === new Peinture());
    
    }



    public function testIsEmpty()
    {
        $commentaire = new Commentaire();

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getBlogpost());
        $this->assertEmpty($commentaire->getPeinture());
        $this->assertEmpty($commentaire->getId());

    }
    
}
